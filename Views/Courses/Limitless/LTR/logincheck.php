<?php
include_once '../../../../vendor/autoload.php';
use Apps\Course\Courses;
  $object=new courses();
  $object->prepare($_POST);
  $num=$object->login();
  if(!is_array($num))
  {
      $_SESSION['authentication']='error';
      header('Location:login.php');
  }
  else
  {
   $_SESSION['email']=$num['email'];
   
   header('Location:index.php');
  }
