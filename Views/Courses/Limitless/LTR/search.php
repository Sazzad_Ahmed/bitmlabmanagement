<?php 
include_once '../../../../vendor/autoload.php';
use Apps\Course\Courses;
  $object=new courses();
  
if(!isset($_SESSION['email']))
{
    header('Location:login.php');
}
else
{
    //print_r($_SESSION);
    $object->prepare($_SESSION);
    $user=$object->databyemail();
   // print_r($user);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>themelock.com - Limitless - Responsive Web Application Kit by Eugene Kopyov</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="assets/js/plugins/visualization/d3/d3.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/styling/switchery.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	<script type="text/javascript" src="assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/pickers/daterangepicker.js"></script>

	<script type="text/javascript" src="assets/js/core/app.js"></script>
	<script type="text/javascript" src="assets/js/pages/Home.js"></script>
	<!-- /theme JS files -->

</head>

<body>

	<!-- Main navbar -->
	<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand" href="index.php"><img src="assets/images/logo.png" alt=""></a>

			<ul class="nav navbar-nav visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>

				
			</ul>

			

			<ul class="nav navbar-nav navbar-right">
				
				

				
<li class="dropdown dropdown-user">

				<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="assets/images/sazzad.jpg" alt="">
						<span><?php echo $user['username'] ?></span>
						<i class="caret"></i>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a ><i class="icon-user-plus"></i>Profile </a></li>
						<li><a ><i class="icon-cog5"></i> Account settings</a></li>
						<li><a href="logout.php"><i class="icon-switch2"></i> Logout</a></li>
					</ul>
                                    </li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">

					<!-- User menu -->
					<div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
								<div class="media-body">
									<span class="media-heading text-semibold">Basis Bangladesh</span>
									<div class="text-size-mini text-muted">
										<i class="icon-pin text-size-small"></i> &nbsp;Dhaka,Bangladesh
									</div>
								</div>

								<div class="media-right media-middle">
									<ul class="icons-list">
										<li>
											<a href="#"><i class="icon-cog3"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- /user menu -->


					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
								<li class="active"><a href="index.php"><i class="icon-home4"></i> <span>Course Informatica</span></a></li>
								<li>
									<a href="#"><i class="icon-stack2"></i> <span>Course Management</span></a>
									
								</li>
								<li>
									<a href="#"><i class="icon-copy"></i> <span>User Management</span></a>
									
								</li>
								<li>
									<a href="#"><i class="icon-droplet2"></i> <span>Software Management</span></a>
									
								</li>
								<li>
									<a href="#"><i class="icon-stack"></i> <span>Training Management</span></a>
									
								</li>
							
								<!-- /main -->





								

							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

			


				<!-- Content area -->
				<div class="content">

					<!-- Main charts -->
					<div class="row">
						<div class="col-lg-9">
								
<a href="Create.php"> Add New</a>
<head>
   
    
</head>
    <?php


  $object=new courses();
  $object->prepare($_POST);
  $row=$object->search();
  
 if((!isset($_GET['start']))&&(!isset($_GET['end'])))
 {
     $start=0;
     $end=10;
    
 }
 else
 {
    if(isset($_GET['start'])){   $start=$_GET['start']-10;
     $end=$start+10;
 }
   if(isset($_GET['end'])){   $start=$_GET['end'];
     $end=$start+10;
 }
 
 }
 $flag=false;
 if(sizeof($row)<=$start)
 {
    
     $flag=true;
 }


?>
<h2> Course Information</h2>
<div class="mywish" style="width: 100%">
<div class="panel panel-flat">
						

						

						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr class="bg-blue">
										<th>Id</th>
										<th>Title</th>
										<th>Duration</th>
										<th>Course_Type</th>
                                                                                <th> Is_Offered</th>
                                                                                <th>Action</th>
									</tr>
								</thead>
								<tbody>
                                                                    <?php
                                                                     $cnt=1;
                                                                     $cnt2=1;
    foreach($row as $singledata) {
         if(($cnt2>$start)&&($cnt2<=$end)){
       ?>
    
									<tr>
										<td><?php echo $cnt++; ?></td>
										<td><?php echo $singledata['title']; ?></td>
                                                                                <td><?php echo intval($singledata['duration']).' Month'; ?></td>
										
                                                                                <td><?php echo $singledata['course_type']?></td>
                                                                                <td>
                                                                                    <?php if($singledata['is_offer']==1)
                                                                                    {
                                                                                        echo 'offered';
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        echo 'stopped';
                                                                                    }
                                                                                        ?>
                                                                                </td>
                                                                                                                                                        <td>

                                                                                                                                                            <button>  <a href="show.php?unique_id=<?php echo $singledata['unique_id']?>">show</a></button>
                                                                                                                                                            <button> <a href="edit.php?unique_id=<?php echo $singledata['unique_id']?>">update</a></button>
                                                                                                                                                            <button>    <a href="delete.php?unique_id=<?php echo $singledata['unique_id']?>" onclick="return confirm('Are you sure?')">delete</a></button>

                                                                              </td>
                                                                        </tr>
									<?php }
    $cnt2++;
    }   ?>
								</tbody>
							</table>
						</div>
					</div>
    </div>
					<!-- /table header styling -->
  

						</div>
                                            <div class="col-lg-3">
                                                <h3 style="float:left">Search<h3>
<form action="search.php" method="POST">
    <input type="search" name="search" required="" placeholder="Search by name">
    <input type="submit" value="clickme">
</form>
                                            </div>
                                            <div class="col-lg-9">
                                                
                                                 <button style="float:left" ><a <?php if($start!=0){ ?>href="search.php?start=<?php echo ($start); }?>">Pre</a></button>
                                                <button style="float:right"><a <?php if(!$flag){ ?>href="search.php?end=<?php echo ($end);}?>">Next</a></button>
                                            </div>

						<div class="col-lg-5">

							

						</div>
					</div>
					<!-- /main charts -->




					<!-- Footer -->
					<div class="footer text-muted">
						&copy; 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>

