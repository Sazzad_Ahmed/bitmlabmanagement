<?php 
include_once '../../../../vendor/autoload.php';
use Apps\Course\Courses;
  $object=new courses();
  
if(!isset($_SESSION['email']))
{
    header('Location:login.php');
}
else
{
   // print_r($_SESSION);
    $object->prepare($_SESSION);
    $user=$object->databyemail();
   // print_r($user);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>themelock.com - Limitless - Responsive Web Application Kit by Eugene Kopyov</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="assets/js/plugins/visualization/d3/d3.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/styling/switchery.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	<script type="text/javascript" src="assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/pickers/daterangepicker.js"></script>

	<script type="text/javascript" src="assets/js/core/app.js"></script>
	<script type="text/javascript" src="assets/js/pages/Home.js"></script>
	<!-- /theme JS files -->

</head>

<body>

	<!-- Main navbar -->
	<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand" href="index.html"><img src="assets/images/logo.png" alt=""></a>

			<ul class="nav navbar-nav visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>

				
			</ul>

			

			<ul class="nav navbar-nav navbar-right">
				
				

				
<li class="dropdown dropdown-user">

				<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="assets/images/sazzad.jpg" alt="">
						<span><?php echo $user['username'] ?></span>
						<i class="caret"></i>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a ><i class="icon-user-plus"></i>Profile </a></li>
						<li><a ><i class="icon-cog5"></i> Account settings</a></li>
						<li><a href="logout.php"><i class="icon-switch2"></i> Logout</a></li>
					</ul>
                                    </li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">

					<!-- User menu -->
					<div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
								<div class="media-body">
									<span class="media-heading text-semibold">Victoria Baker</span>
									<div class="text-size-mini text-muted">
										<i class="icon-pin text-size-small"></i> &nbsp;Santa Ana, CA
									</div>
								</div>

								<div class="media-right media-middle">
									<ul class="icons-list">
										<li>
											<a href="#"><i class="icon-cog3"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- /user menu -->


					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
								<li class="active"><a href="index.php"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
								<li>
									<a href="#"><i class="icon-stack2"></i> <span>Page layouts</span></a>
									<ul>
										<li><a href="layout_navbar_fixed.html">Fixed navbar</a></li>
										<li><a href="layout_navbar_sidebar_fixed.html">Fixed navbar &amp; sidebar</a></li>
										<li><a href="layout_sidebar_fixed_native.html">Fixed sidebar native scroll</a></li>
										<li><a href="layout_navbar_hideable.html">Hideable navbar</a></li>
										<li><a href="layout_navbar_hideable_sidebar.html">Hideable &amp; fixed sidebar</a></li>
										<li><a href="layout_footer_fixed.html">Fixed footer</a></li>
										<li class="navigation-divider"></li>
										<li><a href="boxed_default.html">Boxed with default sidebar</a></li>
										<li><a href="boxed_mini.html">Boxed with mini sidebar</a></li>
										<li><a href="boxed_full.html">Boxed full width</a></li>
									</ul>
								</li>
								<li>
									<a href="#"><i class="icon-copy"></i> <span>Layouts</span></a>
									<ul>
										<li><a href="index.html" id="layout1">Layout 1 <span class="label bg-warning-400">Current</span></a></li>
										<li><a href="../../layout_2/LTR/index.html" id="layout2">Layout 2</a></li>
										<li><a href="../../layout_3/LTR/index.html" id="layout3">Layout 3</a></li>
										<li><a href="../../layout_4/LTR/index.html" id="layout4">Layout 4</a></li>
										<li class="disabled"><a href="../../layout_5/LTR/index.html" id="layout5">Layout 5 <span class="label">Coming soon</span></a></li>
									</ul>
								</li>
								<li>
									<a href="#"><i class="icon-droplet2"></i> <span>Color system</span></a>
									<ul>
										<li><a href="colors_primary.html">Primary palette</a></li>
										<li><a href="colors_danger.html">Danger palette</a></li>
										<li><a href="colors_success.html">Success palette</a></li>
										<li><a href="colors_warning.html">Warning palette</a></li>
										<li><a href="colors_info.html">Info palette</a></li>
										<li class="navigation-divider"></li>
										<li><a href="colors_pink.html">Pink palette</a></li>
										<li><a href="colors_violet.html">Violet palette</a></li>
										<li><a href="colors_purple.html">Purple palette</a></li>
										<li><a href="colors_indigo.html">Indigo palette</a></li>
										<li><a href="colors_blue.html">Blue palette</a></li>
										<li><a href="colors_teal.html">Teal palette</a></li>
										<li><a href="colors_green.html">Green palette</a></li>
										<li><a href="colors_orange.html">Orange palette</a></li>
										<li><a href="colors_brown.html">Brown palette</a></li>
										<li><a href="colors_grey.html">Grey palette</a></li>
										<li><a href="colors_slate.html">Slate palette</a></li>
									</ul>
								</li>
								<li>
									<a href="#"><i class="icon-stack"></i> <span>Starter kit</span></a>
									<ul>
										<li><a href="starters/horizontal_nav.html">Horizontal navigation</a></li>
										<li><a href="starters/1_col.html">1 column</a></li>
										<li><a href="starters/2_col.html">2 columns</a></li>
										<li>
											<a href="#">3 columns</a>
											<ul>
												<li><a href="starters/3_col_dual.html">Dual sidebars</a></li>
												<li><a href="starters/3_col_double.html">Double sidebars</a></li>
											</ul>
										</li>
										<li><a href="starters/4_col.html">4 columns</a></li>
										<li>
											<a href="#">Detached layout</a>
											<ul>
												<li><a href="starters/detached_left.html">Left sidebar</a></li>
												<li><a href="starters/detached_right.html">Right sidebar</a></li>
												<li><a href="starters/detached_sticky.html">Sticky sidebar</a></li>
											</ul>
										</li>
										<li><a href="starters/layout_boxed.html">Boxed layout</a></li>
										<li class="navigation-divider"></li>
										<li><a href="starters/layout_navbar_fixed_main.html">Fixed main navbar</a></li>
										<li><a href="starters/layout_navbar_fixed_secondary.html">Fixed secondary navbar</a></li>
										<li><a href="starters/layout_navbar_fixed_both.html">Both navbars fixed</a></li>
										<li><a href="starters/layout_fixed.html">Fixed layout</a></li>
									</ul>
								</li>
								<li><a href="changelog.html"><i class="icon-list-unordered"></i> <span>Changelog</span></a></li>
								<!-- /main -->





								

							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

			


				<!-- Content area -->
				<div class="content">

					<!-- Main charts -->
					<div class="row">
						<div class="col-lg-12">
								
<a href="Create.php"> Add New</a>
<head>
   
    
</head>
    <?php


  $object->prepare($_GET);
  $singledata=$object->show();
?>


<div class="panel panel-flat">
						

						

				
							
                                                                
                                                                <div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title"><?php ?></h5>
							

						<div class="panel-body">
							

							<div class="content-group-lg">
								<h2 class="text-semibold">CourseId :
                                                               <?php echo $singledata['id']?> </h2>
							</div>

							
						</div>
                                                        <div class="panel-body">
							

							<div class="content-group-lg">
								<h2 class="text-semibold">CourseTitle :
                                                               <?php echo $singledata['title']?> </h2>
							</div>

							
						        </div>
                                                        <div class="panel-body">
							

							<div class="content-group-lg">
								<h2 class="text-semibold">Unique Id :
                                                               <?php echo $singledata['unique_id']?> </h2>
							</div>

							
						</div>
                                                        <div class="panel-body">
							

							<div class="content-group-lg">
								<h2 class="text-semibold">Duration :
                                                               <?php echo $singledata['duration'].'Month Course with'.$singledata['totalhour'].' hours'?> </h2>
                                                                </h2>
							</div>

							
						</div>
                                                       
							

							
								
                                                               
							

							
						
                                                        <div class="panel-body">
							

							<div class="content-group-lg">
								<h2 class="text-semibold">Course Type :
                                                               <?php echo $singledata['course_type']?> </h2>
							</div>

							
						</div>
                                                        <?php 
                                                        if($singledata['course_type']=='paid'){?>
                                                        <div class="panel-body">
							

							<div class="content-group-lg">
								<h2 class="text-semibold">CourseFee :
                                                               <?php echo $singledata['course_fee']?> </h2>
							</div>
                                                            <?php } ?>

							
						</div>
                                                              <div class="panel-body">
							

							<div class="content-group-lg">
								<h2 class="text-semibold">  Is offered :
                                                               <?php
                                                               if($singledata['is_offer']==1)
                                                               {
                                                                   echo 'Offered';
                                                               }
                                                               else
                                                               {
                                                                   echo 'stopped';
                                                               }
                                                               ?> </h2>
							</div>
                                                            

							
						             </div>
                                                        <div class="panel-body">
							

							<div class="content-group-lg">
								<h2 class="text-semibold">  Description :
                                                               
                                                                </h2>
                                                            <p>
                                                                <?php
                                                                    echo $singledata['description']
                                                                ?>
                                                            </p>
							</div>
                                                            

							
						             </div>
                                                        <div class="col-lg-4">

							<div class="panel-body">
							

							<div class="content-group-lg">
								<h3 class="text-semibold"> Created :
                                                               <?php
                                                               echo $singledata['created'];
                                                               ?> </h3>
							</div>
                                                            

							
						             </div>
                                                                        
						</div>
                                            <div class="col-lg-4">

							<div class="panel-body">
							

							<div class="content-group-lg">
								<h3 class="text-semibold"> Updated :
                                                               <?php
                                                               echo $singledata['updated'];
                                                               ?> </h3>
							</div>
                                                            

							
						             </div>
                                                    
						</div>
					</div>
						
						</div>
					
    </div>
					<!-- /table header styling -->
  

						</div>
                     
                                            

						
                                            <div class="col-lg-4">

						<button class="btn-group-xlg bg-green-800 btn-rounded">  <a style="color: white; font-size:25px" href="show.php?unique_id=<?php echo $singledata['unique_id']?>">show</a></button>
                                                                                                                                                            <button class="btn-group-xlg bg-green-800 btn-rounded"> <a style="color: white; font-size:25px" href="edit.php?unique_id=<?php echo $singledata['unique_id']?>">update</a></button>
                                                                                                                                                            <button class="btn-group-xlg bg-green-800 btn-rounded">    <a style="color: white; font-size:25px" href="delete.php?unique_id=<?php echo $singledata['unique_id']?>" onclick="return confirm('Are you sure?')">delete</a></button>	

					     </div>
                                            
					</div>
					<!-- /main charts -->




					<!-- Footer -->
					<div class="footer text-muted">
						&copy; 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
