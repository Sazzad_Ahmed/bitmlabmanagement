<?php 
include_once '../../../../vendor/autoload.php';
use Apps\Course\Courses;
  $object=new courses();
  
if(!isset($_SESSION['email']))
{
    header('Location:login.php');
}
else
{
   // print_r($_SESSION);
    $object->prepare($_SESSION);
    $user=$object->databyemail();
   // print_r($user);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>themelock.com - Limitless - Responsive Web Application Kit by Eugene Kopyov</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="assets/js/plugins/visualization/d3/d3.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/styling/switchery.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	<script type="text/javascript" src="assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/pickers/daterangepicker.js"></script>

	<script type="text/javascript" src="assets/js/core/app.js"></script>
	<script type="text/javascript" src="assets/js/pages/Home.js"></script>
	<!-- /theme JS files -->

</head>

<body>

	<!-- Main navbar -->
	<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand" href="index.php"><img src="assets/images/logo.png" alt=""></a>

			<ul class="nav navbar-nav visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>

				
			</ul>

			

			<ul class="nav navbar-nav navbar-right">
				
				

				<li class="dropdown dropdown-user">

				<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="assets/images/sazzad.jpg" alt="">
						<span><?php echo $user['username'] ?></span>
						<i class="caret"></i>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a ><i class="icon-user-plus"></i>Profile </a></li>
						<li><a ><i class="icon-cog5"></i> Account settings</a></li>
						<li><a href="logout.php"><i class="icon-switch2"></i> Logout</a></li>
					</ul>
                                    </li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">

					<!-- User menu -->
					<div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
								<div class="media-body">
									<span class="media-heading text-semibold">Basis Bangladesh</span>
									<div class="text-size-mini text-muted">
										<i class="icon-pin text-size-small"></i> &nbsp;Dhaka,Bangladesh
									</div>
								</div>

								<div class="media-right media-middle">
									<ul class="icons-list">
										<li>
											<a href="#"><i class="icon-cog3"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- /user menu -->


						<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
								<li class="active"><a href="index.php"><i class="icon-home4"></i> <span>Course Informatica</span></a></li>
								<li>
									<a href="#"><i class="icon-stack2"></i> <span>Course Management</span></a>
									
								</li>
								<li>
									<a href="#"><i class="icon-copy"></i> <span>User Management</span></a>
									
								</li>
								<li>
									<a href="#"><i class="icon-droplet2"></i> <span>Software Management</span></a>
									
								</li>
								<li>
									<a href="#"><i class="icon-stack"></i> <span>Training Management</span></a>
									
								</li>
							
								<!-- /main -->




								

							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

			


				<!-- Content area -->
				<div class="content">

					<!-- Main charts -->
					<div class="row">
						<div class="col-lg-9">



        
            <legend> Course Information Form</legend>
            <form action="Store.php" method="post">
                <fieldset class="content-group">
             
                  <div class="form-group">
				<label class="control-label col-lg-2">Course Title</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" required name ="title"  <?php
                       if(isset($_SESSION['title'])){
                       ?> value="<?php  echo $_SESSION['title'];}
                       ?>"
                                           >
                                    <?php
                       if(isset($_SESSION['error']['title']))
                       {echo $_SESSION['error']['title'];}
                       ?>
				</div>
	     </div>
                    
               <br/><br/> 
               <div class="form-group">
		<label class="control-label col-lg-2">Description</label>
		<div class="col-lg-10">
		<textarea rows="5" cols="5" class="form-control" name="description"> <?php
                       if(isset($_SESSION['description'])){
                        echo $_SESSION['description'];}
                       ?></textarea>
		</div>
		</div>
               
                <?php
                       if(isset($_SESSION['error']['description']))
                       {echo "<p>".$_SESSION['error']['description']."</p>";}
                  ?>
               
              
               <div class="form-group">
	<label class="control-label col-lg-2">Course Duration</label>
		<div class="col-lg-10">
	<select name="duration" class="form-control">
              
                  <option value="1">1 month</option>
                   <option value="1.5">1.5 month</option>
                    <option value="2">2 month</option> 
                    <option value="2.5">2.5 month</option>
                     <option value="3">3 month</option>
                      <option value="3.5">3.5 month</option>
              </select>
                   
				                          
			                           
			                        </div>
              
              </div>
                 <br/><br/>
                 
                
                        <div class="form-group" >
				<label class="control-label col-lg-2">Total hours</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" placeholder="120 hours" name="totalhours" >
                                    
                      
				</div>
	                   </div>
                <br/><br/>  
                 <div class="form-group">
	<label class="control-label col-lg-2">Course Duration</label>
		<div class="col-lg-10">
	<select  name="coursetype" class="form-control">
                      <option value="paid">Paid</option>
                      <option value="unpaid">unpaid</option>
                  </select>
                </div>
                 </div>
                 <div class="form-group">
				<label class="control-label col-lg-2">Course fee</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control"  name ="coursefee" >
                                    <?php
                       if(isset($_SESSION['error']['coursefee']))
                       {echo $_SESSION['error']['coursefee'];}
                       ?>
				</div>
	     </div>
                 
                  
                 <br/><br/> 
                 <div class="form-group">
                     <label class="control-label col-lg-2">Is active</label>
              <div class="col-lg-3">
                   <select  name="isactive" class="form-control">
                      <option value="1">Active</option>
                      <option value="0" >Inactive</option>
                    
                  </select>
                  </div>
                 </div>
                 <br/><br/>
                 
                  <div class="form-group">
                     <label class="control-label col-lg-2">Is offer</label>
              <div class="col-lg-4">
                 <select name="isoffer" class="form-control">
                     <option value="1">Offered</option>
                     <option value="0">stopped</option>
                 </select>
              </div></div>
                 
               <?php
                $login=$_SESSION['email'];
               $_SESSION=array();
               $_SESSION['email']=$login;
               ?>
                 
                 
                     <div class="form-group">
                     <label class="control-label col-lg-2"></label>
                     <div class="col-lg-3">
                  <input type="submit" value="click me">
                    </div></div>
                    </fieldset>
            </form>
      
    

						</div>
                                            
                                            

						<div class="col-lg-5">

							

						</div>
					</div>
					<!-- /main charts -->




					<!-- Footer -->
					<div class="footer text-muted">
						&copy; 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
