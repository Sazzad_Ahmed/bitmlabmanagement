<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitb379be621c81d70eba6a92f3602b5f54
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'Apps\\' => 5,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Apps\\' => 
        array (
            0 => __DIR__ . '/../..' . '/Src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitb379be621c81d70eba6a92f3602b5f54::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitb379be621c81d70eba6a92f3602b5f54::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
