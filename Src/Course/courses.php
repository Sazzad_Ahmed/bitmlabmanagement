<?php
namespace Apps\Course;
use PDO;
class courses {
    
    public $title,$duration,$description,$totalhours,$hoursperday,$daysperweek,$coursetype,$coursefee,$isactive,$isoffer;
    
       public $unique_id,$conn,$search,$email,$password;         
       public function __construct() {
        @session_start();
        $host = 'localhost';
        $user = 'root';
        $password = '';

        $db = 'bitm';

        $this->conn = new PDO("mysql:host=$host;dbname=$db", "$user", "$password");
    }
    public function prepare($data) {
        
         //print_r($data);
         if(array_key_exists('email', $data))
         {
             $this->email=$data['email'];
         }
         if(array_key_exists('password', $data))
         {
             $this->password=$data['password'];
         }
          if(array_key_exists('unique_id', $data))
         {
             $this->unique_id=$data['unique_id'];
         }
         if(array_key_exists('title', $data))
         {
             $this->title=$data['title'];
         }
          if(array_key_exists('duration', $data))
         {
             $this->duration=$data['duration'];
         }
          if(array_key_exists('description', $data))
         {
             $this->description=$data['description'];
         }
          if(array_key_exists('totalhours', $data))
         {
             $this->totalhours=$data['totalhours'];
         }
          if(array_key_exists('hoursperday', $data))
         {
             $this->hoursperday=$data['hoursperday'];
         }
          if(array_key_exists('daysperweeeks', $data))
         {
             $this->daysperweeeks=$data['daysperweeeks'];
         }
          if(array_key_exists('coursetype', $data))
         {
             $this->coursetype=$data['coursetype'];
             if($this->coursetype=='paid')
             {
                 $this->coursefee=$data['coursefee'];
             }
             else
             {
                  $this->coursefee=0;
             }
         }
         
          if(array_key_exists('isactive', $data))
         {
             $this->isactive=$data['isactive'];
         }
         if(array_key_exists('isoffer', $data))
         {
             $this->isoffer=$data['isoffer'];
         }
         if(array_key_exists('search', $data))
         {
             $this->search=$data['search'];
         }
         //echo $this->unique_id;

         return $this;
    }
     public function store() {



        try {
            $sql = "INSERT INTO `courses`(`id`,`unique_id`,`title`,`duration`,`description`,`course_type`,`course_fee`,`is_offer`,`created`,`totalhour`)
        VALUES(:i,:ui,:un,:ps,:em,:vr,:ia,:cr,:tr,:th)";
            $query = $this->conn->prepare($sql);
            $query->execute(array(
                ':i' => NULL,
                ':ui' => uniqid(),
                ':un' => $this->title,
                ':ps' => $this->duration,
                ':em' => $this->description,
                ':vr' =>$this->coursetype ,
                ':ia' => $this->coursefee,
                ':cr' => $this->isoffer,
                ':tr'=>date('Y-m-d-h-i-s'),
                ':th'=> $this->totalhours
            ));
            
          header('Location:index.php');
        } catch (Exception $ex) {
           
        }
    }
    public function titleexist()
    {
         $sql = $this->conn->prepare("SELECT `title` FROM `courses` WHERE `title`='$this->title'");
        $sql->execute();
        return $sql->rowCount();
        
    }
   public function index() {
      

        
     
        $sql = $this->conn->prepare("SELECT * FROM `courses` ORDER BY `created` DESC");
        $sql->execute();
      
        while ($result = $sql->fetch(PDO::FETCH_ASSOC)) {
            $this->row[] = $result;
        }

        return $this->row;
    }
 

    public function delete() {
        echo $this->unique_id;

        
      $sql = "DELETE FROM `courses` WHERE `unique_id`=  :filmID";
$stmt = $this->conn->prepare($sql);
$stmt->bindParam(':filmID', $this->unique_id, PDO::PARAM_INT);   
$stmt->execute();
   
    }
     public function show() {
       // echo $this->uniqe_id;
        $sql = $this->conn->prepare("SELECT * FROM `courses` WHERE `unique_id`='$this->unique_id'");
        $sql->execute();
        while ($result = $sql->fetch(PDO::FETCH_ASSOC)) {
            return $result;
        }
    }
    public function  update()
    {
        $sql = "UPDATE courses SET title = :fn,duration = :dr,description= :des,course_type= :ct,course_fee= :cf,is_offer=:io,updated=:up WHERE unique_id = :un"; 
        $query = $this->conn->prepare($sql);
            $query->execute(array(
                ':fn' => $this->title,
                ':dr'=>  $this->duration,
                ':des'=> $this->description,
                ':ct'=>  $this->coursetype,
                ':cf'=>  $this->coursefee,
                ':io'=>  $this->isoffer,
                ':up'=>date('Y-m-d-h-i-s'),
                ':un'=>$this->unique_id
                                    

            ));
        //header('Location:index.php');
    }
  public function search()
  {
     
      $query = 'SELECT * FROM courses WHERE title LIKE :search';

$stmt= $this->conn->prepare($query);

$stmt->execute(array(':search' => '%'.$this->search.'%'));

$result = $stmt->fetchAll(PDO::FETCH_ASSOC);


return $result;
  }
  public function  numrowbyname()
  {
      
       $sql = $this->conn->prepare("SELECT * FROM `courses` WHERE `title`='$this->title'");
        $sql->execute();
        return $sql->rowCount();
  }
  public function login()
  {
      $sql = $this->conn->prepare("SELECT * FROM `users` WHERE `email`='$this->email' AND `password`='$this->password'");
     
       $sql->execute();
       $result = $sql->fetch(PDO::FETCH_ASSOC);
       if($sql->rowCount()!=0)
       {
           return $result;
       }
       else
       {
           return 0;
       }
            
        
  }
  public function databyemail()
          
  {
      $sql = $this->conn->prepare("SELECT * FROM `users` WHERE `email`='$this->email'");
     
       $sql->execute();
       $result = $sql->fetch(PDO::FETCH_ASSOC);
       return $result;
  }
    
}
